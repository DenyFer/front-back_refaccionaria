import { Component, OnInit } from '@angular/core';
import {  Sevicio1Service} from '../../../servicios/peticiones/sevicio1.service';
import { FormujsService } from 'src/app/servicios/peticiones/formujs.service';


@Component({
  selector: 'app-compo1',
  templateUrl: './compo1.component.html',
  styleUrls: ['./compo1.component.css']
})

export class Compo1Component implements OnInit {
  lado=4;
  //listapersonal:any  [] = [{nombrePersonal:"",apellidoPersonal:"userSchema.apellidoPersonal",rolPersonal:"userSchema.rolPersonal",telefonoPersonal:"userSchema.telefonoPersonal",direccionPersonal:"userSchema.direccionPersonal",edadPersonal:'',id:"id"}]
 datos:any
 //listaproductos:any  [] = [{nombreProducto:"",marcaProducto:"userSchema.marcaProducto",presentacionProducto:"userSchema.presentacionProducto",contenidoProducto:"userSchema.contenidoProducto",cantidadIngresa:"userSchema.cantidadIngresa",statusProducto:"userSchema.statusProducto",descripcionProducto:"userSchema.descripcionProducto"}]
 productos:any
 //listasucursal:any  [] = [{nombreSucursal:"",direccionSucursal:"userSchema.direccionSucursal",telefonoSucursal:"userSchema.telefonoSucursal",horarioSucursal:"userSchema.horarioSucursal",marcasqmanejaSucursal:"userSchema.marcasqmanejaSucursal"}]
 sucursal:any
 //listaproveedores:any  [] = [{nombreProveedor:"",direccionSucursal:"userSchema.direccionSucursal",telefonoSucursal:"userSchema.telefonoSucursal",horarioSucursal:"userSchema.horarioSucursal",marcasqmanejaSucursal:"userSchema.marcasqmanejaSucursal"}]
 proveedor:any
  mostrar = false;


  constructor(private _service:Sevicio1Service   ) {
   }
  ngOnInit(): void {
    this.consultaUsuario();
    this.consultaProducto();
    this.consultaSucursal();
    this.consultaProveedores();
  }
  consultaPersonal(){
    this._service.consulta()
    .subscribe((resp:any) => {
      console.log(resp.respuesta);
      this.datos = resp.respuesta;
    })
  }

  consultaUsuario(){
    this._service.consultaus()
    .subscribe((resp:any) => {
      console.log(resp.respuesta);
      this.datos = resp.respuesta;
    })
  }

  consultaProducto(){
    this._service.consultaProductos()
    .subscribe((resp:any)=>{
      console.log(resp.respuesta);
      this.productos=resp.respuesta;
    })
  }

  consultaSucursal(){
    this._service.consultaSucur()
    .subscribe((resp:any)=>{
      console.log(resp.respuesta);
      this.sucursal=resp.respuesta;
    })
  }
  consultaProveedores(){
    this._service.consultaProvee()
    .subscribe((resp:any)=>{
      console.log(resp.respuesta);
      this.proveedor=resp.respuesta;
    })
  }
}

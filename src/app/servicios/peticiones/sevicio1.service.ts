import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class Sevicio1Service {
  private URL="http://localhost:3900";

    constructor(private _http:HttpClient){

    }
    consultaper(){
      const url=`${this.URL}/personal/:id`;
      return this._http.get(url);
      console.log(url);
    }
    consulta(){
      const url=`${this.URL}/personal`;
      return this._http.get(url);
      console.log(url);
    }


    consultaus(){
      const url=`${this.URL}/obtener/personal`;
      return this._http.get( url );
      console.log(url);
     }

     consultaProductos(){
      const url=`${this.URL}/productos`;
      return this._http.get( url );
      console.log(url);
     }
     consultaSucur(){
      const url=`${this.URL}/sucursal`;
      return this._http.get( url );
      console.log(url);
     }

     consultaProvee(){
      const url=`${this.URL}/proveedor`;
      return this._http.get( url );
      console.log(url);
     }








}
